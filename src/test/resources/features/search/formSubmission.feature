Feature: feature to test form submission
  Scenario: Check whether user is able to submit form
    Given user is on the webpage
    When user fills up the form
    Then user submits the form