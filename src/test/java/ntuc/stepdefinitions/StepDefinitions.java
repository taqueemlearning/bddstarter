package ntuc.stepdefinitions;


import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;


import pages.NTUCAssignmentPage;


public class StepDefinitions extends NTUCAssignmentPage {


    @Given("user is on the webpage")
    public void user_is_on_the_webpage() {
        navigateToAssignmentPage();
    }
    @When("user fills up the form")
    public void user_fills_up_the_form() {

    }
    @Then("user submits the form")
    public void user_submits_the_form() {

    }


}
